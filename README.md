# pag-proj-frontend
Frontend para pag_proj. Executa na porta 3000 na máquina local.

## Requisitos
- Node.js e NPM, versões mais recentes;
- Servidor pag_prog executando na porta 8083 na máquina local.

## Como executar (desenvolvimento)
- Execute `npm install`;
- Execute `npm start` para iniciar o servidor web de desenvolvimento.

## Considerações

- A lista de pessoas é fixa, embora o backend está preparado para armazenar a lista de pessoas no banco de dados;
- Embora o backend suporte dividir um mesmo item entre mais de uma pessoa, a interface limita a uma pessoa por item.
- Embora o backend também permita escolher a pessoa que arcará com a diferença extra em caso de divisões não-exatas (como total 100/3, por exemplo), no frontend foi só utilizado o comportamento padrão (sortear aleatoriamente a pessoa que arcará com essa sobra)
- A categoria é retornada do servidor. Utilizar `product` para produtos consumidos, `delivery` para taxa de entrega.