import * as React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { IMaskInput } from "react-imask";
import { Autocomplete, Button, Dialog, DialogActions, DialogContent, DialogTitle, FormGroup, FormLabel, List, ListItem, TextField } from '@mui/material';
import useFetch from './utils';

const RootContext = React.createContext<{categories?: Category[]}>({categories: []});

export default function App() {
  const [bill, setBill] = React.useState<Bill>({title: '', items: []});
  const [billId, setBillId] = React.useState<string|number>();
  const [creatingBill, setCreatingBill] = React.useState(false);
  const [creatingPayment, setCreatingPayment] = React.useState(false);
  const [payments, setPayments] = React.useState<BillPayment[]|undefined>();
  const {data, error} = useFetch<Category[]>('http://localhost:8083/bill-item-categories');

  const createPayments = (uid: string) => {
    setCreatingPayment(true);
    fetch(`http://localhost:8083/bills/${uid}/payments`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
    })
      .then(async res => {
        const body = await res.json() as BillPayment[];
        setPayments(body);
        setCreatingPayment(false);
      })
      .catch(_ => {setCreatingPayment(false); setPayments(undefined)});
  }

  const saveBill = (bill: Bill) => {
    setCreatingBill(true);
    fetch(`http://localhost:8083/bills${billId ? `/${billId}` : ''}`, {
      method: billId ? 'PUT' : 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(bill)
    })
      .then(async res => {
        const body = await res.json() as Bill;
        setBill(body);
        setBillId(body.id)
        createPayments(body.uniqueId!);
        setCreatingBill(false);
      })
      .catch(err => {
        setCreatingBill(false);
      });
  }

  const isBillInvalid = React.useMemo(() => {
    if (!bill.title) {
      return true;
    }

    if (!bill.items.length) {
      return true;
    }

    for (const billItem of bill.items) {
      if (!billItem.name) {
        return true;
      }

      if (!billItem.category) {
        return true;
      } 

      if (billItem.category.personNeeded && (!billItem.persons || !billItem.persons.length)) {
        return true;
      }
    }

    return false;
  }, [bill]);

  return (<RootContext.Provider value={{categories: data}}>
      <Container maxWidth="md">
        <Typography variant='h3'>Dividir Despesa</Typography>
        <Box sx={{ my: 4 }}>
          <EditBill value={bill} onChange={setBill} />
        </Box>
        <Button onClick={(e) => saveBill(bill)} variant="contained" disabled={!data || creatingBill || creatingPayment || isBillInvalid}>
          {billId ? 'Atualizar' : 'Criar'} e Calcular
        </Button>
        {payments && <Payments payments={payments} />}
      </Container>
    </RootContext.Provider>
  );
}

export function Payments({payments}: {payments: BillPayment[]}) {
  return <Box mt={3}>
    <hr />
    <Typography variant='h5'>Cotas de Pagamento</Typography>
    {payments?.map((payment, i) => <PaymentItem key={i} payment={payment} />)}
  </Box>;
}

export function PaymentItem({payment}: {payment: BillPayment}) {
  return <Box border="1px solid lightgray" p={2} mt={2}>
    <Typography variant='h6'>{payment.person?.name}</Typography>
    <Typography fontSize='1.125rem'>Total a Pagar: R${payment.personTotal}</Typography>
    {payment.extraneousPayment && <Typography fontSize='0.813rem'>(pagamento extra devido à divisão não-exata, sorteado automaticamente: R${payment.extraneousPayment})</Typography> || ''}
    {Object.entries(payment.paymentProviders ?? {}).map(([providerName, provider], i) =>
      <PaymentItemPayment key={i} payment={payment} providerName={providerName} provider={provider} />
    )}
  </Box>
}

export function PaymentItemPayment(
  {payment, providerName, provider}: {payment: BillPayment, providerName: string, provider: BillPaymentProvider}
) {
  const hasModal = React.useMemo(() => Object.keys(provider.fields).length, [provider.fields]);
  const providerFields = React.useMemo(() => Object.entries(provider.fields), [provider.fields]);
  const [providerFieldValues, setProviderFieldValues] = React.useState<{[key: string]: any}>({});
  const [modalOpen, setModalOpen] = React.useState(false);
  const [paymentLink, setPaymentLink] = React.useState('');

  const getPaymentLink = (providerName: string, fields?: {[key: string]: any}) => {
    fetch(`http://localhost:8083/bills/${payment.bill?.uniqueId}/payments/${payment.uniqueId}/link?provider=${providerName}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(fields ?? {})
    })
      .then(async res => {
        const body = await res.json() as {link: string}
        setPaymentLink(body.link);
      })
      .catch(() => setPaymentLink(''))
  }

  const doPayment = () => {
    setModalOpen(false);
    if (hasModal) {
      setModalOpen(true);
    } else {
      getPaymentLink(providerName);
    }
  }

  const doParseFields = () => {
    getPaymentLink(providerName, providerFieldValues);
    setModalOpen(false);
  }

  return <>
    <Button onClick={doPayment}>Pagar com {provider.title}</Button>
    {paymentLink}
    {hasModal && <Dialog
      open={modalOpen}
      onClose={() => setModalOpen(false)}
    >
      <DialogTitle>Gerar link de pagamento</DialogTitle>
      <DialogContent>
        {providerFields?.map(([fieldName, field], i) => {
          if (field.type == 'string') {
            return <TextField
              label={field.title}
              variant="outlined"
              size="small"
              value={providerFieldValues[fieldName]}
              helperText={field.description}
              onChange={(e) => setProviderFieldValues({...providerFieldValues, [fieldName]: e.target.value})} />
          }
        })}
      </DialogContent>
      <DialogActions>
        <Button onClick={() => setModalOpen(false)}>Cancelar</Button>
        <Button onClick={() => doParseFields()}>Gerar</Button>
      </DialogActions>
    </Dialog>}
  </>;
}

export function EditBill({value, onChange}: {value: Bill, onChange: (value: Bill) => void}) {
  const removeItem = (i: number) => {
    const newItems = [...value.items];
    newItems.splice(i, 1);
    onChange({...value, items: newItems});
  }

  const updateItem = (item: BillItem, i: number) => {
    const newItems = [...value.items];
    newItems[i] = item;
    onChange({...value, items: newItems});
  }

  return <>
    <TextField label="Nome da Fatura" variant="outlined" value={value.title} onChange={(e) => onChange({...value, title: e.target.value})} />
    <List>
      {value.items && value.items.map((item, i) =>
        <ListItem key={i}>
          <BillItemEdit value={item} onChange={(item) => updateItem(item, i)} onRemove={() => removeItem(i)} />
        </ListItem>
      )}
    </List>
    <Button onClick={(e) => onChange({...value, items: [...value.items, {name: '', persons: [], price: 0, category: null, percent: false}]})}>
      + Adicionar Despesa
    </Button><br />
  </>
}

interface CustomProps {
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

const MoneyOrPercentageComponent = React.forwardRef<HTMLElement, CustomProps>(
  function TextMaskCustom(props, ref) {
    const { onChange, ...other } = props;
    return (
      <IMaskInput
        {...other}
        lazy={false}
        normalizeZeros={false}
        mask="num{[%]}"
        definitions={{
          '%': /%/,
        }}
        blocks={{
          num: {
            mask: Number,
            radix: '.',
            mapToRadix: [',']
          }
        }}
        inputRef={ref}
        onAccept={(value: any) => onChange({ target: { name: props.name, value } })}
        overwrite
      />
    );
  },
);

const PERSONS: Person[] = [
  {name: "Pessoa 1", uniqueId: "27db85e5-2e62-4df7-a676-10cbb94e9171"},
  {name: "Pessoa 2", uniqueId: "bea6188a-42c0-4174-aac6-3d5940cfb016"},
  {name: "Pessoa 3", uniqueId: "91b1dc81-b57b-4733-bac0-140bc6c10dbc"},
  {name: "Pessoa 4", uniqueId: "a2bea9ac-373b-421f-86b4-71f3981cba9b"},
  {name: "Pessoa 5", uniqueId: "a4113ba6-81de-4e73-b4c9-04bc5995f963"},
  {name: "Pessoa 6", uniqueId: "a2a82ed6-ff97-453f-952b-8af1dbfe0e6d"},
]

export function BillItemEdit({value, onChange, onRemove}: {value: BillItem, onChange: (value: BillItem) => void, onRemove: () => void}) {
  const {categories} = React.useContext(RootContext);
  const price = React.useMemo(() => value.price + `${value.percent ? '%' : ''}`, [value.price, value.percent]);
  return <Box border="1px solid lightgray" p={2}>
    <TextField label="Nome da Despesa" variant="outlined" size="small" value={value.name} onChange={(e) => onChange({...value, name: e.target.value})} />
    <TextField
      label="Valor"
      variant="outlined"
      size="small"
      value={price}
      onChange={(e) => {
        onChange({...value, price: e.target.value.replace('%', ''), percent: e.target.value.endsWith("%")})
      }}
      InputProps={{
        inputComponent: MoneyOrPercentageComponent as any
      }}
    />
    <Autocomplete
      options={PERSONS}
      getOptionLabel={(item) => item.name}
      value={value.persons?.[0] ?? undefined}
      onChange={(e, selectedPerson) => onChange({...value, persons: selectedPerson ? [selectedPerson] : []})}
      renderInput={(params) => <TextField {...params} label="Pessoa" />}
    />
    <Autocomplete
      options={categories ?? []}
      disabled={!categories}
      getOptionLabel={(item) => item.name}
      value={value.category ?? undefined}
      onChange={(e, category) => onChange({...value, category: category!})}
      renderInput={(params) => <TextField {...params} label="Categoria" />}
    />
    <Button color="error" onClick={() => onRemove()}>Remover</Button>
  </Box>
}

interface Category {
  id?: string;
  name: string;
  uniqueId: string;
  discount: boolean;
  personNeeded: boolean;
}

interface Bill {
  id?: string;
  uniqueId?: string;
  title: string;
  items: BillItem[]
}

interface BillItem {
  id?: string;
  uniqueId?: string;
  name: string;
  persons: Person[];
  price: string|number;
  percent: boolean;
  category: Category|null;
}

interface Person {
  id?: string;
  name: string;
  uniqueId?: string;
}

interface BillPayment {
  id?: number;
  uniqueId?: string;
  person: Person;
  bill?: Bill;
  stake: string;
  personTotal: string;
  billTotal: string;
  paymentProviders?: BillPaymentProviders;
  paymentProvider: string|null;
  extraneousPayment: string|null;
}

type BillPaymentProviders = {[key: string]: BillPaymentProvider};

interface BillPaymentProvider {
  title: string;
  fields: {
    [key: string]: {
      title: string;
      description: string;
      type: 'string';
      required: boolean;
    }
  }
}